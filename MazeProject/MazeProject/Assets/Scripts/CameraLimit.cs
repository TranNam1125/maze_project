﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraLimit : MonoBehaviour
{
    [SerializeField]
    Transform target;
    

    void Update()
    {
        transform.position = new Vector3(Mathf.Clamp(target.position.x, 0.4f, 4.6f), Mathf.Clamp(target.position.y, -5.6f, -2.4f), transform.position.z);
    }
}
