﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseButton : MonoBehaviour
{
    public GameObject pausePannel;
    bool pause;

    public void PauseMenu()
    {
        pause = !pause;
        pausePannel.SetActive(pause);
    }
    public void LoadScene()
    {
        SceneManager.LoadScene(1);
    }

    public void LoadMenu()
    {
        SceneManager.LoadScene(0);
    }
}
