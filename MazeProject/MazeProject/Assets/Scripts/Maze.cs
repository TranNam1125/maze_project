﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Maze : MonoBehaviour
{
    public GameObject wall;
    public int xRows = 5;
    public int yCollums = 5;

    void Start()
    {
        float size = wall.transform.localScale.x;
        for(int i = 0; i < xRows; ++i)
        {
            for(int j = 0; j < yCollums; ++j)
            {
                Instantiate(wall, new Vector2(j, i), Quaternion.identity);
            }
        }
    }
    
    void Update()
    {
        
    }
}
