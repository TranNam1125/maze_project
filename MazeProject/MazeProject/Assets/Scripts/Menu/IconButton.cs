﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class IconButton : MonoBehaviour
{
    public GameObject rightStar;
    public GameObject leftStar;
    public GameObject midStar;
    public GameObject lockImage;
    public GameObject tutImage;
    public bool lockLv = false;
    public int currentLevelButton;


    public void UnLockLevel(bool unLockLV)
    {
        if (unLockLV)
        {
            lockImage.SetActive(false);
            RandomStar(Random.Range(0, 4));
            lockLv = true;
        }
        else
            return;
    }

    public void RandomStar(int start)
    {
        switch (start)
        {
            case 1:
                leftStar.SetActive(true);
                break;
            case 2:
                leftStar.SetActive(true);
                rightStar.SetActive(true);
                break;
            case 3:
                leftStar.SetActive(true);
                rightStar.SetActive(true);
                midStar.SetActive(true);
                break;
        }
    }

     void LoadLevel()
    {
        if(lockLv)
            SceneManager.LoadScene(1);
    }
}
