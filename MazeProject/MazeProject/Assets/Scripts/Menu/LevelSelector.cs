﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class LevelSelector : MonoBehaviour
{
    IconButton IconButton;
    Grid gridScript;
    public GameObject levelHolder;
    public GameObject levelIcon;
    public GameObject thisCanvas;
    public int numberOfLevels = 50;
    private Rect iconDimensions;
    private Rect pannelDimensions;
    private int amountPerPage;
    private int currentLevelCount;


    void Start()
    {
        pannelDimensions = levelHolder.GetComponent<RectTransform>().rect;
        iconDimensions = levelIcon.GetComponent<RectTransform>().rect;
        //int maxInARow = Mathf.FloorToInt(pannelDimensions.width / iconDimensions.width);
        //int maxInACol = Mathf.FloorToInt(pannelDimensions.height / iconDimensions.height);
        //int amountPerPage = maxInARow * maxInACol;
        //int totalPages = Mathf.CeilToInt((float)numberOfLevels / amountPerPage);
        int totalPages = Mathf.CeilToInt((float)numberOfLevels / 20);
        LoadPannels(totalPages);
    }

    void LoadPannels(int numberOfPannels)
    {
        //Debug.Log(numberOfPannels);
        GameObject pannelClone = Instantiate(levelHolder) as GameObject;
        levelHolder.AddComponent<PageSwiper>();

        for(int i = 1; i <= numberOfPannels; i++)
        {
            GameObject pannel = Instantiate(pannelClone) as GameObject;
            pannel.transform.SetParent(thisCanvas.transform, false);
            pannel.transform.SetParent(levelHolder.transform);
            pannel.name = "page " + i;
            pannel.GetComponent<RectTransform>().localPosition = new Vector2(0, pannelDimensions.height * (i - 1));
            SetUpGrid(pannel);
            int numberOfIcon = i == numberOfPannels ? numberOfLevels - currentLevelCount : amountPerPage;
            LoadIcons(20, pannel);
        }
        Destroy(pannelClone);
    }

    void LoadIcons(int numberOfIcon, GameObject parrentObject)
    {
        int levelAt = PlayerPrefs.GetInt("levelAt", 1);
        for (int i = 1; i <= numberOfIcon; i++)
        {
            currentLevelCount++;
            GameObject icon = Instantiate(levelIcon) as GameObject;
            icon.transform.SetParent(thisCanvas.transform, false);
            icon.transform.SetParent(parrentObject.transform);
            icon.name = "icon " + i;
            IconButton iconScript = icon.GetComponentInChildren<IconButton>();
            if(currentLevelCount == 1)
            {
                iconScript.tutImage.SetActive(true);
                iconScript.lockImage.SetActive(false);
                icon.GetComponentInChildren<TextMeshProUGUI>().SetText("");
            }
            else
            {
                iconScript.tutImage.SetActive(false);
                icon.GetComponentInChildren<TextMeshProUGUI>().SetText("" + currentLevelCount);
            }

            if(currentLevelCount + 1 > levelAt)
            {
                iconScript.UnLockLevel(false);
            }
            else
            {
                iconScript.UnLockLevel(true);
            }

            iconScript.currentLevelButton = currentLevelCount;
        }
    }

    void SetUpGrid(GameObject pannel)
    {
        GridLayoutGroup grid = pannel.AddComponent<GridLayoutGroup>();
        grid.spacing = new Vector2(100f, 130f);
        grid.startCorner = GridLayoutGroup.Corner.LowerLeft;
        grid.childAlignment = TextAnchor.LowerCenter;

    }
}
